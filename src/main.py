from yages_schema_pb2_grpc import EchoStub
import grpc
from yages_schema_pb2 import Empty
import concurrent.futures

if __name__ == "__main__":
    channel = grpc.insecure_channel('localhost:63491')
    stub = EchoStub(channel=channel)

    with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:
        for i in range(1200):
            executor.submit(lambda : stub.Ping(Empty()) )
        # for i in range(1200):
        #     stub.Ping(
        #         Empty()
        #     )
        #     print(i)
    